/**
 * @file registers the fullscreen toolbar button and binds functionality to it.
 */

import {
  Plugin
} from 'ckeditor5/src/core';
import {
  ButtonView
} from 'ckeditor5/src/ui';
import icon from '../../../../icons/fullscreen-big.svg';
import iconCancel from '../../../../icons/fullscreen-cancel.svg';

export default class FullscreenUI extends Plugin {

  init() {
    const editor = this.editor;

    // This will register the fullscreen toolbar button.
    editor.ui.componentFactory.add('fullscreen', locale => {
      const buttonView = new ButtonView(locale);
      const editorRegion = editor.sourceElement.nextElementSibling;
      const html = document.getElementsByTagName('html')[0];
      let state = 0;
      // Callback executed once the image is clicked.
      buttonView.set({
        label: 'Full screen',
        icon: icon,
        tooltip: true
      });
      buttonView.on('execute', () => {
        if (state == 1) {
          editorRegion.removeAttribute('data-fullscreen');
          editorRegion.removeAttribute('data-offset-top');
          html.style.removeProperty('--drupal-displace-offset-top');
          html.style.removeProperty('--drupal-displace-offset-left');
          html.style.removeProperty('--drupal-displace-offset-right');
          document.body.removeAttribute('data-fullscreen');
          buttonView.set({
            label: 'Full screen',
            icon: icon,
            tooltip: true
          });
          state = 0;
        } else {
          editorRegion.setAttribute('data-fullscreen', 'fullscreeneditor');
          editorRegion.setAttribute('data-offset-top', '');
          html.style.setProperty('--drupal-displace-offset-top', '0px', 'important');
          html.style.setProperty('--drupal-displace-offset-left', '0px', 'important');
          html.style.setProperty('--drupal-displace-offset-right', '0px', 'important');
          document.body.setAttribute('data-fullscreen', 'fullscreenoverlay');
          buttonView.set({
            label: 'Mode Normal',
            icon: iconCancel,
            tooltip: true
          });
          state = 1;
        }
      });
      return buttonView;
    });
  }
}
